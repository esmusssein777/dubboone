package com.ligz.dubbo.one.model.mapper;

import com.ligz.dubbo.one.model.entity.OrderRecord;

/**
 * author:ligz
 */
public interface OrderRecordMapper {
    int insertSelective(OrderRecord orderRecord);

    OrderRecord selectByPrimaryKey(Integer id);
}
