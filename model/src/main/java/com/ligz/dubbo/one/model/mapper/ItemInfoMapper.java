package com.ligz.dubbo.one.model.mapper;

import com.ligz.dubbo.one.model.entity.ItemInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * author:ligz
 */
public interface ItemInfoMapper {
    ItemInfo selectByPrimaryKey(Integer id);

    List<ItemInfo> selectAll();

    List<ItemInfo> selectAllByParams(@Param("search") String search);
}
