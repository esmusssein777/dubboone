package com.ligz.dubbo.one.api.service;


import com.ligz.dubbo.one.api.request.PushOrderDto;
import com.ligz.dubbo.one.api.response.BaseResponse;

/**
 * author:ligz
 */
public interface IDubboRecordService {
    //下单
    public BaseResponse pushOrder(PushOrderDto pushOrderDto);
}
