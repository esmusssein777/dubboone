package com.ligz.dubbo.one.api.service;


import com.ligz.dubbo.one.api.response.BaseResponse;

/**
 * author:ligz
 */
public interface IDubboItemService {
    //列出商品
    public BaseResponse listItems();
    //分页列出商品
    public BaseResponse listPageItems(Integer pageNo, Integer pageSize);
    //分页查询商品
    public BaseResponse listPageItemsParams(Integer pageNo, Integer pageSize, String search);
}
